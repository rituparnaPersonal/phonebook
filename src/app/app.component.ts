import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Phone Book';
  firstName:any='Coder';
  lastName:any='Byte';
  phoneNumber:any='8885559999';
  phonebokList:any=[]

  add(){
    if(this.firstName== undefined || this.firstName == null || this.firstName == ''){
      alert('Please enter first name.');
    }
    else if(this.lastName== undefined || this.lastName == null || this.lastName == ''){
      alert('Please enter last name.');
    }
    else if(this.phoneNumber== undefined || this.phoneNumber == null || this.phoneNumber == ''){
      alert('Please enter phone number.');
    }else{
    
    this.phonebokList.push({
    'firstName':this.firstName,
    'lastName':this.lastName,
    'phoneNumber':this.phoneNumber
    })
  
    this.phonebokList.sort((a:any,b:any)=>{
      let lNa = a.lastName.toLowerCase();
      let lNb = b.lastName.toLowerCase();
  
      if (lNa < lNb) {
          return -1;
      }
      if (lNa > lNb) {
          return 1;
      }
      return 0;
        })
    }
    
  }
}
